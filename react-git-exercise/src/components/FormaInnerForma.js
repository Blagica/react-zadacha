import React, { Component } from 'react';
    	
class FormaInnerForma extends Component {
	render() {
		return (
			<div className="FormaInnerForma">
				<form>
					<label htmlFor="name">Your Name</label>
					<input type="text" name="name" className="form-control" />
					<label htmlFor="email">Email Address</label>
					<input type="text" name="email" className="form-control" />
					<label htmlFor="phone">Phone</label>
					<input type="text" name="phone" className="form-control" />
					<label htmlFor="message">Your Message</label>
					<textarea name="message" className="form-control" rows="5"></textarea>
					<button className="btn btn-primary">Email Us</button>
				</form>
			</div>
		);
	}
}

export default FormaInnerForma;
