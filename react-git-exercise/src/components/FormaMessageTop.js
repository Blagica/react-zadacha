import React from 'react';

const FormaMessageTop = (props) => {
  return (
    <div className="FormaMessageTop">
    	<h3>Send us a message</h3>
    	<p>You can contact us with anything related to React</p>
    	<p>We'll get in touch with you as soon as possible</p>
    </div>
  )
}

export default FormaMessageTop;