import React from 'react';

const FormaAdressTop = (props) => {
  return (
    <div className="FormaAdressTop">
    	<h4 style={{marginTop : "20px"}}>Our address</h4>
    	<p>The Old Road Willington, <br/> 7 Kings Road <br/> SouthShore,64890</p>
    </div>
  )
}

export default FormaAdressTop;