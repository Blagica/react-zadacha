import React, { Component } from 'react';
    	
class Navbar extends Component {
	render() {
		return (
			<div className="Navbar">
				<nav className="nav">
					<div className="container">
						<div className="row">
							<div className="col-md-10 col-md-offset-1">
								<div className="navbar-header">
									<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-button" aria-expanded="false">
				        					<span className="sr-only">Toggle navigation</span>
				        					<span className="icon-bar"></span>
				        					<span className="icon-bar"></span>
				        					<span className="icon-bar"></span>
				      				</button>
									<a href="/" className="navbar-brand"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/640px-React-icon.svg.png" alt="" style={{width: "50px", display: "inline", marginTop: "-7px"}}/>React</a>
								</div>
								<div className="collapse navbar-collapse" id="menu-button">
									<ul className="nav navbar-nav navbar-right">
										<li><a href="/">Home Pages <span className="caret"></span></a></li>
										<li><a href="/">Showcase <span className="caret"></span></a></li>
										<li><a href="/">Pricing <span className="caret"></span></a></li>
										<li><a href="/">More Pages <span className="caret"></span></a></li>
										<li><a href="/">Components <span className="caret"></span></a></li>
										<li><a href="/">Blog <span className="caret"></span></a></li>
										<li>
											<button href="/" className="dropdown-toggle" data-toggle="dropdown" id="nav-button">Sign Up </button>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</nav>
			</div>
		);
	}
}

export default Navbar;
