import React from 'react';

const FormaChat = (props) => {
  return (
    <div className="FormaChat">
    	<div className="well">
	    	<h4>Looking for online support?</h4>
	    	<p className="talk">Talk to us now with our online chat.</p>
    	</div>
		<i className="fa fa-facebook fa-2x" aria-hidden="true"></i>
		<i className="fa fa-twitter fa-2x" aria-hidden="true"></i>
    </div>
  )
}

export default FormaChat;