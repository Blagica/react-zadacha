import React, { Component } from 'react';
import FormaMessageTop from "./FormaMessageTop.js";
import FormaInnerForma from "./FormaInnerForma.js";
import FormaAdressTop from "./FormaAdressTop.js";
import FormaPhone from "./FormaPhone.js";
import FormaChat from "./FormaChat.js";
    	
class Forma extends Component {
	render() {
		return (
			<div className="Forma">
				<div className="row">
					<div className="col-md-6">
						<FormaMessageTop />
						<FormaInnerForma />
					</div>
					<div className="col-md-3 col-md-offset-3">
						<FormaAdressTop />
						<FormaPhone />
						<FormaChat />
					</div>
				</div>
				
			</div>

		);
	}
}

export default Forma;
