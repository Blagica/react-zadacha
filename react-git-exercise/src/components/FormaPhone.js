import React from 'react';

const FormaPhone = (props) => {
  return (
    <div className="FormaPhone">
    	<h4 style={{marginTop: "20px"}}>By Phone</h4>
    	<p>1-800-346-3444</p>
    </div>
  )
}

export default FormaPhone;