import React, { Component } from 'react';
import './App.css';
import Caruzel from "./Components/Caruzel.js"
import Caption from "./Components/Caption.js"
import Navbar from "./components/Navbar.js";
import Forma from "./components/Forma.js";

class App extends Component {
  render() {
    return (
      <div className="App">  
       <Navbar />
       <Caruzel />
        <Caption />
        <div className="container">
           <div className="row">
              <div className="col-md-10 col-md-offset-1">
                 <Forma />
              </div>
          </div>
        </div>
        </div>

    );
  }
}

export default App;
