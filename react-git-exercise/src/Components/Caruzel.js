
import React, {Component} from 'react';

class Caruzel extends Component {
	render() {
		return (
			<div className="Caruzel">
				<div className="container-fluid" style={{paddingLeft:"0px",paddingRight:"0px"}}>
					<div id="myCarousel" className="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" className="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>
						<div className="carousel-inner">
							<div className="item active" style={{position:"relative"}}>
								<img src="https://images.pexels.com/photos/417122/pexels-photo-417122.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="Los Angeles" style={{width: "100%", height:"100vh"}}/>
								<div className="carousel-caption" style={{position:"absolute",top:"35vh"}}>
									<div className="row">
										<div className="col-md-6 text-left">
											<h2 style={{marginBottom:"20px"}}>Take control of your business anywhere</h2>
											<p style={{marginBottom:"20px"}}>React is the best way to get a great design out of the box with lots of options for customization.</p>
											<button className="btn btn-default">TRY IT FREE</button>
										</div>
									</div>
								</div>
							</div>
							<div className="item" style={{position:"relative"}}>
								<img src="https://images.pexels.com/photos/861121/pexels-photo-861121.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="Chicago" style={{width: "100%", height:"100vh"}}/>
								<div className="carousel-caption" style={{position:"absolute",top:"35vh"}}>
									<div className="row">
										<div className="col-md-6 text-left">
											<h2 style={{marginBottom:"20px"}}>Take control of your business anywhere</h2>
											<p style={{marginBottom:"20px"}}>React is the best way to get a great design out of the box with lots of options for customization.</p>
											<button className="btn btn-default">TRY IT FREE</button>
										</div>
									</div>
								</div>
							</div>
							<div className="item" style={{position:"relative"}}>
								<img src="https://images.pexels.com/photos/1466971/pexels-photo-1466971.jpeg?auto=compress&cs=tinysrgb&h=650&w=940" alt="New york" style={{width: "100%", height:"100vh"}}/>
								<div className="carousel-caption" style={{position:"absolute",top:"35vh"}}>
									<div className="row">
										<div className="col-md-6 text-left">
											<h2 style={{marginBottom:"20px"}}>Take control of your business anywhere</h2>
											<p style={{marginBottom:"20px"}}>React is the best way to get a great design out of the box with lots of options for customization.</p>
											<button className="btn btn-default">TRY IT FREE</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="#myCarousel" className="left carousel-control" data-slide="prev">
							<span className="glyphicon glyphicon-chevron-left"></span>
							<span className="sr-only">Previous</span>
						</a>
						<a href="#myCarousel" className="right carousel-control" data-slide="next">
							<span className="glyphicon glyphicon-chevron-right"></span>
							<span className="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		);
	}
}

export default Caruzel;
