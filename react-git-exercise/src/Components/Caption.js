
import React, {Component} from 'react';

class Caption extends Component {
	render() {
		return (
			<div className="Caption">
				<div className="container-fluid">
					<div className="row">
						<div className="col-md-12" style={{textAlign:"center", margin:"40px 0"}}>
							<h2>Need an easy way to customize your site?</h2>
							<h4 className="text-muted">React is perfect for novice developers and experts alike.</h4>
						</div>
					</div>
					<div className="row">
						<div className="col-md-10 col-md-offset-2">
							<div className="row" style={{margin:"40px 0"}}>
								<div className="col-md-6" style={{height:"40vh", display:"flex", justifyContent:"center", alignItems:"center"}}>
									<div>
										<h3 style={{color:"lightblue",marginTop:"0px"}}>You don't need to have great technical experience to use your product.</h3>
										<p className="text-muted" style={{lineHeight: "25px"}}>Whether you want to fill this paragraph with some text like i'm doing right now.This place is perfect to descibe some features or anything you want - React has a complete solution for your.</p>
									</div>
								</div>
								<div className="col-md-5" style={{height:"40vh"}}>
									<img src="https://thegoodguys.sirv.com/products/50060611/50060611_566594.PNG?scale.height=505&scale.width=773&canvas.height=505&canvas.width=773&canvas.opacity=0&format=png&png.optimize=true" style={{width:"400px"}}/>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Caption;
